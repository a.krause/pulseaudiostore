import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Ofert {

    private  String bandName;
    private String albumName;
    private int year;
    private boolean phiscal;
    private boolean digital;

}
// a.krause